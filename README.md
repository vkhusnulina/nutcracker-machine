# NUTCRACKER-MACHINE #

Web site, *actually offline*. Designed to the [MDECA Group Companies](http://www.mdecagroup.com/en/main.html)

### Details ###

* Developed on **ASP.NET** language
* Front-end with **HTML/CSS**
* Edited [video](https://www.youtube.com/watch?v=qFcCJiZfzTM) presentation

![Capture.PNG](https://bitbucket.org/repo/zzA8aB/images/671231733-Capture.PNG)
![Capture2.PNG](https://bitbucket.org/repo/zzA8aB/images/2875263871-Capture2.PNG)
![Capture3.PNG](https://bitbucket.org/repo/zzA8aB/images/3221330716-Capture3.PNG)
![Capture4.PNG](https://bitbucket.org/repo/zzA8aB/images/3708907113-Capture4.PNG)