﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPage.Master" CodeBehind="index.aspx.vb" Inherits="CascadoraDeNueces.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <li><a href="index.aspx" class="current">Inicio</a></li>
    <li><a href="caracteristicas.aspx" target="_parent">Caracter&iacute;sticas</a></li>
    <li><a href="videos.aspx" target="_parent">Videos</a></li>
    <li><a href="empresa.aspx" target="_parent">Nuestra Empresa</a></li>
    <li><a href="contacto.aspx" target="_parent">Contacto</a></li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section_w860">	
        <div class="section_w380 float_l">
            <h2>Ahorr&aacute; Tiempo y Dinero!</h2>
            <div class="image_wrapper_01"><img src="images/index_1.jpg" alt="image" width="350" height="150" /></div>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;La m&aacute;quina  hace el trabajo de rotura de la c&aacute;scara,  que se suele realizar de forma manual &quot;a martillito&quot; con un considerable gasto de tiempo, y una separaci&oacute;n gruesa, que depende de la variedad  y de lo secas que est&eacute;n las nueces.</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;En t&eacute;rminos generales, <b>ahorra entre un 60 y un 70% del trabajo total del pelado</b>, donde solo la separaci&oacute;n final debe hacerse en forma manual.</p>
        </div>
        <div class="section_w380 float_r">
            <h2>Beneficios de Cascadora de Nueces </h2>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;Estas m&aacute;quinas son ideales para los peque&ntilde;os y medianos <strong>productores de frutos secos de Argentina</strong>. Son  robustas y confiables, para un <strong>uso continuo</strong>.</p>
            <ul class="service">
                <li><b>R&aacute;pida y econ&oacute;mica.</b></li>
                <li><b>Un resultado impecable.</b></li>
                <li><b>El mantenimiento es m&iacute;nimo</b> y se reduce a mantener la limpieza y lubricaci&oacute;n del eje de la placa al fin de la temporada.</li>
                <li>No es necesario el tama&ntilde;ado previo de las nueces.</li>
                <li><b>Tama&ntilde;o muy c&oacute;modo</b> para transportar y mover la m&aacute;quina, entra en cualquier camioneta pequeña.</li>
            </ul>   
            <div class="cleaner_h20"></div>
            <div class="button_01"><a href="caracteristicas.aspx">Leer m&aacute;s</a></div>
        </div>
    </div> 
</asp:Content>
