﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPage.Master" CodeBehind="contacto.aspx.vb" Inherits="CascadoraDeNueces.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder2" >
    <li><a href="index.aspx" target="_parent">Inicio</a></li>
    <li><a href="caracteristicas.aspx" target="_parent">Caracter&iacute;sticas</a></li>
    <li><a href="videos.aspx" target="_parent">Videos</a></li>
    <li><a href="empresa.aspx" target="_parent">Nuestra Empresa</a></li>
    <li><a href="contacto.aspx" class="current">Contacto</a></li>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section_w860">	
        <div class="section_w380 float_l">
            <h2>Formulario de contacto</h2>
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            
        </div>
        <div class="section_w380 float_r">
            <h2>Datos de contacto</h2>
            <ul class="service">
                <li><b>Tel./fax:</b> +54-11-4861-9769</li>
                <li><b>Email:</b>mdeca@nutcracker-machine.com<br />vavidi2012@yahoo.com.ar</li>
                <li><b>Skype:</b> (Ing. Rinat Khusnulin)<br />rinat681</li>
                <li><b>Domicilio postal – Departamento comercial:</b><br />Gasc&oacute;n 1448, Piso 1 "C"<br />
                                                                           Capital Federal (1181)<br />
                                                                           Buenos Aires<br />
                                                                           Argentina</li>
            </ul>
        </div>
    </div>    
</asp:Content>
