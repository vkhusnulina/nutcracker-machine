﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPage.Master" CodeBehind="empresa.aspx.vb" Inherits="CascadoraDeNueces.empresa" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <li><a href="index.aspx" target="_parent">Inicio</a></li>
    <li><a href="caracteristicas.aspx" target="_parent">Caracter&iacute;sticas</a></li>
    <li><a href="videos.aspx" target="_parent">Videos</a></li>
    <li><a href="empresa.aspx" class="current">Nuestra Empresa</a></li>
    <li><a href="contacto.aspx" target="_parent">Contacto</a></li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section_w860">
        <div class="section_w380 float_l">
            <h2>Sobre Nosotros</h2>	
            <div class="image_wrapper_01">
                <a href="http://mdecagroup.com">
                    <img src="images/empresa_1.jpg" alt="image" width="347" height="96"  class="img_link" />
                </a>
            </div>		
            <p>&nbsp;&nbsp;&nbsp;&nbsp;El Grupo de Compa&ntilde;&iacute;as Sudamericanas &quot;Movimiento de Capital&quot; (<strong>MDECA Group</strong>) fue fundado en 2002 como asociaci&oacute;n de empresas agropecuarias y financieras de Argentina, Paraguay y Uruguay, especializ&aacute;ndose en la producci&oacute;n y exportaci&oacute;n de alimentos y materia prima a muchos pa&iacute;ses del mundo, as&iacute; como tambi&eacute;n en la importaci&oacute;n de productos qu&iacute;micos al Mercosur.</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;MDECA Group posee producci&oacute;n propia en Argentina y Paraguay y colabora con m&aacute;s de 30 cooperativas regionales – productoras de alimentos. La sede comercial est&aacute; ubicada en Buenos Aires, Argentina.</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;Gracias a buena calidad de nuestros productos y trabajo eficaz <b>tenemos clientes permanentes en Argentina, los E.E.U.U., Rusia, Ucrania, Corea, Turqu&iacute;a, Israel, China y otros pa&iacute;ses.</b></p>
        </div>
        <div class="section_w380 float_r">
            <h2>Somos Distribuidor Oficial</h2>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;En nuestra oficina <b>disponemos de la m&aacute;quina cascadora de nueces</b> para posibilitar la demostraci&oacute;n del funcionamiento de la misma.</p>
            <p>&nbsp;&nbsp;&nbsp;&nbsp;<i>Para ello, pongase en </i><a href="contacto.aspx">contacto con nosotros</a> <i>para coordinar la reuni&oacute;n.</i></p>
            <ul class="service">
                <li>
                    <p>&nbsp;&nbsp;&nbsp;&nbsp;<b>Nuestra p&aacute;gina oficial:</b> <a href="http://mdecagroup.com">mdecagroup.com</a></p>
                </li>
            </ul>
            <div class="cleaner_h20"></div>
            <div class="button_01"><a href="contacto.aspx">Contacto</a></div>
        </div>
    </div>
</asp:Content>
