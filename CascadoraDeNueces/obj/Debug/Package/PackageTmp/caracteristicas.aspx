﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPage.Master" CodeBehind="caracteristicas.aspx.vb" Inherits="CascadoraDeNueces.caracteristicas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <li><a href="index.aspx" target="_parent">Inicio</a></li>
    <li><a href="caracteristicas.aspx" class="current">Caracter&iacute;sticas</a></li>
    <li><a href="videos.aspx" target="_parent">Videos</a></li>
    <li><a href="empresa.aspx" target="_parent">Nuestra Empresa</a></li>
    <li><a href="contacto.aspx" target="_parent">Contacto</a></li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="section_w860">
    <div class="section_w380 float_l">			
        <h2>Rendimiento según algunas variedades</h2>			
        <ul class="service">
            <li><strong>Chandler y similares</strong>: 95% mariposa</li>
            <li><strong>Nuez Criolla</strong>: 80% mariposa</li>
            <li><strong>Pec&aacute;n</strong>: 55% mariposa</li>
        </ul>			
        <p>El porcentaje sobrante en todos los casos es pr&aacute;cticamente <b>todo medio mariposa.</b></p>
        <br />
        <p>Tambi&eacute;n la m&aacute;quina es usada para pelar:</p>				
        <ul class="service">  
            <li><strong>Almendra</strong></li>
            <li><strong>Casta&ntilde;a</strong></li>
            <li><strong>Avellana</strong></li>
            <li><strong>Man&iacute;</strong></li>
            <li><strong>Pistacho</strong></li>
            <li><strong>Carozo de Durazno</strong></li>
            <li>..etc.</li>
        </ul>
        <br />
        <div class="image_wrapper_01">
            <img src="images/caracteristicas_1.jpg" alt="image" width="350" height="150" />
        </div>
    </div>
    <div class="section_w380 float_r">			
        <h2>Caracter&iacute;sticas</h2>			
        <div class="image_wrapper_01">
            <img src="images/caracteristicas_2.jpg" alt="image" width="350" height="300" />
        </div>
        <ul class="service">
            <li>Capacidad de procesamiento: <b>500 Kg / hora</b>.</li>
            <li>Los movimientos est&aacute;n montados sobre bolilleros blindados.</li>
            <li>Motor de 1 HP Monof&aacute;sico.</li>
            <li>Tama&ntilde;o: X,Y,Z</li>
        </ul>
        <hr />
        <ul class="service">
            <li><b>Env&iacute;amos a todo el interior y exterior del pa&iacute;s.</b></li>
            <li><b>Garant&iacute;a de 1 a&ntilde;o.</b></li>
        </ul>        
        <div class="cleaner_h20"></div>
        <div class="button_01"><a href="videos.aspx">Ver Videos</a></div>
    </div>		
</div>
</asp:Content>
