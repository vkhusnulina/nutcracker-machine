﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MPage.Master" CodeBehind="error.aspx.vb" Inherits="CascadoraDeNueces._error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <li><a href="index.aspx" target="_parent">Inicio</a></li>
    <li><a href="caracteristicas.aspx" target="_parent">Caracter&iacute;sticas</a></li>
    <li><a href="videos.aspx" target="_parent">Videos</a></li>
    <li><a href="empresa.aspx" target="_parent">Nuestra Empresa</a></li>
    <li><a href="contacto.aspx" class="current">Contacto</a></li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="section_w860">	
        <div class="section_w380 float_l">
            <h2>Error</h2>
            <p>Se ha produccido un error inesperado.</p>
            <p>Vuelva a intentar luego. Disculpe las molestias.</p>
            <div class="cleaner_h20"></div>
            <div class="button_01"><a href="contacto.aspx">Volver</a></div>
        </div>
</div>
</asp:Content>
