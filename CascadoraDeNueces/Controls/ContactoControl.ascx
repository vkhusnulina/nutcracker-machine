﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ContactoControl.ascx.vb" Inherits="CascadoraDeNueces.ContactoControl" %>
<asp:Panel ID="Panel1" runat="server" DefaultButton="btnEnviar">
<table>
<!--NOMBRE-->
    <tr>
        <td align="right" class="td_txt">
            <p><asp:Label ID="lblNombre" runat="server" Text="Nombre:*"></asp:Label></p>
        </td>
        <td>
            <asp:TextBox ID="txtNombre" runat="server" Width="250px" MaxLength="20" onKeyPress="javascript:presiona(event)"></asp:TextBox>
        </td>
    </tr>
<!--EMPRESA-->
    <tr>
        <td align="right">
            <p>Empresa:</p></td>
        <td>
            <asp:TextBox ID="txtEmpresa" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
        </td>
    </tr>
    
<!--EMAIL-->
    <tr>
        <td align="right">
            <p><asp:Label ID="lblEmail" runat="server" Text="Email:* "></asp:Label></p>
        </td>
        <td>
            <asp:TextBox ID="txtEmail" runat="server" Width="250px" MaxLength="30"></asp:TextBox>
        </td>
    </tr>
    
<!--ASUNTO-->
    <tr>
        <td align="right">
            <p>Asunto:</p>
        </td>
        <td>
            <asp:TextBox ID="txtAsunto" runat="server" MaxLength="30" Width="250px"></asp:TextBox>
        </td>
    </tr>
<!--MENSAJE-->
    <tr>
        <td align="right">
            <p><asp:Label ID="lblMensaje" runat="server" Text="Mensaje:* "></asp:Label></p>
        </td>
        <td>
            <asp:TextBox ID="txtMensaje" runat="server" Height="120px" Rows="6" 
            TextMode="MultiLine" Width="250px" MaxLength="2000"></asp:TextBox>
        </td>
    </tr>
    
<!--PAIS-->
    <tr>
        <td align="right">
            <p>Pa&iacute;s:</p>
        </td>
        <td>
            <asp:TextBox ID="txtPais" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
        </td>
    </tr>
        
<!--CIUDAD-->
    <tr>
        <td align="right">
            <p>Ciudad:</p>
        </td>
        <td>
            <asp:TextBox ID="txtCiudad" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
        </td>
    </tr>
    
<!--TELEFONO-->
    <tr>
        <td align="right">
            <p>Tel&eacute;fono:</p>
        </td>
        <td>
            <asp:TextBox ID="txtTel" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
        </td>
    </tr>
    
<!--NOTA FINAL-->
    <tr>
        <td colspan="2" align="center">
            <p><b>Todos los campos marcados con * son obligatorios.</b></p>
        </td>
    </tr>
<!--ENVIAR-->
    <tr>
        <td colspan="2" align="center">
            <asp:Button ID="btnEnviar" runat="server" class="button_01" 
            Text="Enviar" BackColor="#CDD8E1" BorderColor="#0E2F38" BorderStyle="Double" 
                BorderWidth="3px" Font-Names="Times New Roman" Font-Size="12pt" 
                ForeColor="#0E2F38" />
        </td>
    </tr>
</table>
</asp:Panel>
