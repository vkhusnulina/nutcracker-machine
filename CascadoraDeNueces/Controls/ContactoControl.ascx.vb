﻿Imports System.Net.Mail

Public Class ContactoControl
    Inherits System.Web.UI.UserControl


    'Public Property Asunto() As List(Of Object)
    'Get
    '    Return Me.txtAsunto.DataSource
    'End Get
    'Set(ByVal value As List(Of Object))
    '    For Each item As Object In value
    '        Me.txtAsunto.Items.Add(item)
    '    Next

    'End Set
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Sub Enviar_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEnviar.Click
        If ValidarCampos() Then
            Dim mensaje As New StringBuilder()
            mensaje.AppendLine("Nombre: " + Me.txtNombre.Text)
            If (Me.txtEmpresa.Text <> "") Then
                mensaje.AppendLine("Empresa: " + Me.txtEmpresa.Text)
            End If
            If (Me.txtTel.Text <> "") Then
                mensaje.AppendLine("Telefono: " + Me.txtTel.Text)
            End If
            If (Me.txtPais.Text <> "") Then
                mensaje.AppendLine("Pais: " + Me.txtPais.Text)
            End If
            If (Me.txtCiudad.Text <> "") Then
                mensaje.AppendLine("Ciudad: " + Me.txtCiudad.Text)
            End If
            If (Me.txtAsunto.Text = "") Then
                Me.txtAsunto.Text = "Cascadora de nueces"
            End If
            mensaje.AppendLine("Mensaje: " + Me.txtMensaje.Text)

            Enviar(Me.txtEmail.Text, Me.txtAsunto.Text, mensaje.ToString)

            'Response.Redirect("~/Agradecimiento.aspx")
        End If


    End Sub

    Protected Function ValidarCampos() As Boolean
        Dim valido As Boolean = True
        If Me.txtNombre.Text = "" Then
            Me.lblNombre.ForeColor = Drawing.Color.Red
            valido = False
        Else
            Me.lblNombre.ForeColor = Drawing.Color.Black
        End If
        If Me.txtEmail.Text = "" Then
            Me.lblEmail.ForeColor = Drawing.Color.Red
            valido = False
        Else
            Me.lblEmail.ForeColor = Drawing.Color.Black
        End If
        If Me.txtMensaje.Text = "" Then
            Me.lblMensaje.ForeColor = Drawing.Color.Red
            valido = False
        Else
            Me.lblMensaje.ForeColor = Drawing.Color.Black
        End If
        Return valido
    End Function
    Public Sub Enviar(ByVal emailfrom As String, ByVal tema As String, ByVal mensaje As String)
        Dim email As New MailMessage(emailfrom, "mdeca@nutcracker-machine.com", tema, mensaje)
        Dim smtp As New SmtpClient

        Try
            smtp.Send(email)
            Response.Redirect("~/agradecimiento.aspx")
        Catch ex As Exception
            '    Response.Redirect("~/error.aspx")
        End Try
    End Sub
End Class
